package com.parse.starter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class Quest_Details extends Activity {

    private TextView txtQuestName;
    private TextView txtDetail;
    private ImageView imgQuest;

    private GoogleMap googleMap;
    private MarkerOptions marker;
    private CameraPosition cameraPos;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quest_details);
        txtDetail=(TextView)findViewById(R.id.txtDetails);
        imgQuest=(ImageView)findViewById(R.id.imageView);
        txtQuestName=(TextView)findViewById(R.id.txtQuestName);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        try{initMap();}catch (Exception e){e.printStackTrace();}
        new showImage().execute();
        showDetails();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initMap();
    }

    private void initMap() {
        if(googleMap==null){
            googleMap=((MapFragment)getFragmentManager().findFragmentById(R.id.fragmentMap)).getMap();
            googleMap.setMyLocationEnabled(true);
            if(googleMap==null){
                Toast.makeText(this,"Unable to create the map!",Toast.LENGTH_LONG).show();
            }
            else showPositions();
        }
        else showPositions();
    }

    protected  void showPositions(){
        final double latitude=getIntent().getDoubleExtra("latitude",0);
        final double longitude=getIntent().getDoubleExtra("longitude",0);

       // Toast.makeText(this,String.valueOf(latitude),Toast.LENGTH_LONG).show();

        marker=new MarkerOptions().position(new LatLng(latitude, longitude)).title("Here I am!");
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).alpha(0.7f);
        cameraPos=new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))
                .zoom(3).build();
        googleMap.addMarker(marker);
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPos));
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Uri location = Uri.parse("geo:"+Double.toString(latitude)+","+Double.toString(longitude)+"?z=10");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);
                startActivity(mapIntent);
                return true;
            }
        });
    }

    private void showDetails(){
        Intent intent=getIntent();
        String description=(String)intent.getStringExtra("describe");
        String questName=(String)intent.getStringExtra("questName");
        txtQuestName.setText(questName);
        txtDetail.setText(description);
    }

    class showImage extends AsyncTask<Void, Void, Bitmap>{

        @Override
        protected Bitmap doInBackground(Void... urls) {
            Intent intent=getIntent();
            String image=(String)intent.getStringExtra("image");
            URL url= null;
            try {
                url = new URL(image);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            InputStream stream= null;
            try {
                stream = url.openStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap bitmap=BitmapFactory.decodeStream(stream);
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            imgQuest.setImageBitmap(bitmap);
        }
    }
}


package com.parse.starter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.parse.starter.R;

public class signIn extends Activity {

    private EditText edtUsername;
    private EditText edtPassword;
    private EditText edtPasswordAgain;
    private EditText edtName;
    private EditText edtAlignment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ParseAnalytics.trackAppOpened(getIntent());
        edtUsername=(EditText)findViewById(R.id.edtUsername);
        edtPassword=(EditText)findViewById(R.id.edtPassword);
        edtPasswordAgain=(EditText)findViewById(R.id.edtPasswordAgain);
        edtName=(EditText)findViewById(R.id.edtName);
        edtAlignment=(EditText)findViewById(R.id.edtAlignment);
        signUp();
    }

    private void signUp(){
        findViewById(R.id.btnSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUpUser();
            }
        });
    }

    protected  void signUpUser(){
        final String username=edtUsername.getText().toString().trim();
        final String password=edtPassword.getText().toString().trim();
        String passwordAgain=edtPasswordAgain.getText().toString().trim();
        String name=edtName.getText().toString().trim();
        int alignment=Integer.parseInt(edtAlignment.getText().toString());
        if((TextUtils.isEmpty(username))||(TextUtils.isEmpty(password))||(TextUtils.isEmpty(passwordAgain))||
                (TextUtils.isEmpty(name))){
            Toast.makeText(this,"All fields must be filled!",Toast.LENGTH_LONG).show();

        }
        else{
            ParseUser user=new ParseUser();
            user.setUsername(username);
            user.setPassword(password);
            user.put("name",name);
            user.put("alignment",alignment);
            if(password.equals(passwordAgain)){
                user.signUpInBackground(new SignUpCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e==null){
                            ParseUser.logInInBackground(username,password,new LogInCallback() {
                                @Override
                                public void done(ParseUser parseUser, ParseException e) {
                                    if(e==null){
                                        startActivity(new Intent(signIn.this,Populate_Quests.class));
                                        edtPassword.setText("");
                                        edtPasswordAgain.setText("");
                                        edtName.setText("");
                                        edtUsername.setText("");
                                    }
                                }
                            });
                        }
                    }
                });
            }
            else{
                Toast.makeText(getApplicationContext(),"Passwords do not match",Toast.LENGTH_LONG).show();
                edtPassword.setText("");
                edtPasswordAgain.setText("");
            }

        }
    }
}

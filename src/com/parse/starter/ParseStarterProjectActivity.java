package com.parse.starter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseUser;

import org.w3c.dom.Text;

public class ParseStarterProjectActivity extends Activity {
    private EditText edtUsername;
    private EditText edtPassword;
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		ParseAnalytics.trackAppOpened(getIntent());
        edtPassword=(EditText)findViewById(R.id.edtPassword_);
        edtUsername=(EditText)findViewById(R.id.edtUsername_);

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginToSystem();
            }
        });

        findViewById(R.id.btnSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ParseStarterProjectActivity.this,signIn.class));
            }
        });
	}


    protected void loginToSystem(){
        String username=edtUsername.getText().toString().trim();
        String password=edtPassword.getText().toString().trim();
        if(TextUtils.isEmpty(username)){
            edtUsername.setError("Invalid Username");
            edtPassword.setText("");
        }
        else if(TextUtils.isEmpty(password)){
            edtPassword.setError("Invalid Password");
            edtPassword.setText("");
        }
        else{
            ParseUser.logInInBackground(username,password,new LogInCallback() {
                @Override
                public void done(ParseUser parseUser, ParseException e) {
                    if(e==null){
                        startActivity(new Intent(ParseStarterProjectActivity.this,Populate_Quests.class));
                        edtPassword.setText("");
                        edtUsername.setText("");
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Invalid username or password",Toast.LENGTH_LONG).show();
                        edtPassword.setText("");
                    }
                }
            });
        }

    }
}

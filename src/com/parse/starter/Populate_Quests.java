package com.parse.starter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.starter.R;

import java.util.ArrayList;
import java.util.List;

public class Populate_Quests extends Activity {

    private double latitude, longitude;
    private ParseGeoPoint location;
    private ListView listView;
    private  String details;
    ArrayList<String> newTitle=new ArrayList<String>();
    ProgressDialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ParseAnalytics.trackAppOpened(getIntent());
        getActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_populate_quests);
        backGroungLoad();
        setImage();

    }

    private void setImage() {
       /* ParseUser user=ParseUser.getCurrentUser();
        ParseQuery<ParseObject> query=new ParseQuery<ParseObject>("User");
        query.whereEqualTo("username",user);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if(e==null){
                    for(ParseObject object: parseObjects){
                        ParseObject x=object.getParseObject("iamgeUrl");
                        ParseFile file=(ParseFile)x.
                    }
                    ParseFile imgFile=(ParseFile)parseObjects.get()
                }
            }
        });
        String userImg=query.get("imageUrl").toString();
        ParseUser userImg=new ParseUser();
        ParseFile imgFile=(ParseFile)userImg.get("imageUrl");*/
        ParseImageView img=(ParseImageView)findViewById(R.id.icon);
        img.setPlaceholder(getResources().getDrawable(R.drawable.placeholder));
        //img.setParseFile(imgFile);
        //img.loadInBackground();
    }


    private void backGroungLoad() {
        ParseQueryAdapter.QueryFactory<ParseObject> factory=new ParseQueryAdapter.QueryFactory<ParseObject>(){
            @Override
            public ParseQuery<ParseObject> create() {
                ParseQuery query=new ParseQuery("Quests");
                query.whereExists("name");
                return query;
            }
        };

        ParseQueryAdapter<ParseObject> adapter=new ParseQueryAdapter<ParseObject>(this,factory);
        adapter.setTextKey("name");
        //adapter.setImageKey("locationImageUrl");
        adapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<ParseObject>() {
            ProgressDialog progress;
            @Override
            public void onLoading() {
                progress=new ProgressDialog(Populate_Quests.this);
                progress.setMessage("Loading...");
                progress.show();
            }

            @Override
            public void onLoaded(final List<ParseObject> parseObjects, Exception e) {
                if(e==null){
                    progress.cancel();
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            location=parseObjects.get(i).getParseGeoPoint("location");
                            latitude = location.getLatitude();
                            longitude =location.getLongitude();
                            Intent intent = new Intent(Populate_Quests.this, Quest_Details.class);
                            intent.putExtra("describe",parseObjects.get(i).getString("description"));
                            intent.putExtra("image",parseObjects.get(i).getString("locationImageUrl"));
                            intent.putExtra("questName", parseObjects.get(i).getString("name"));
                            intent.putExtra("latitude",latitude);
                            intent.putExtra("longitude",longitude);
                            startActivity(intent);
                            return;
                        }
                    });
                }

            }
        });

        listView=(ListView)findViewById(R.id.quest_list);
        listView.setAdapter(adapter);
    }


}
